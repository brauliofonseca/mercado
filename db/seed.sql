/* Seed data to populate tables with demo user data
 * Must run initTables.sql before running this script
 */
INSERT INTO users (
	userId,
 	username,
 	stockHoldingId, 
	currencyHoldingId,
 	metalsHoldingId) VALUES (
	'0',
	'mercado-demo-user',
	'0',
	'0',
	'0'
);

INSERT INTO stockPosition(
	stockHoldingId,
	ticker,
	averageCost,
	initialBuyDate,
	lastUpdateDate,
	totalAmount) VALUES (
	'0',
	'SPY',
	298.0500,
	'2019-01-02 06:30:44',
	'2019-01-02 06:30:44',
	15
);

INSERT INTO stockChangeRecord(
	stockHoldingId,
	ticker,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'SPY',
	298.0500,
	15,
	'2019-01-02 06:30:44'
);

INSERT INTO metalsPosition(
	metalsHoldingId,
	metalName,
	averageCost,
	initialBuyDate,
	lastUpdateDate,
	totalAmount /* Quantity is measured in oz */
	) VALUES (
	'0',
	'SILVER',
	15.6700,
	'2019-01-02 06:45:32',
	'2019-01-02 06:45:32',
	10
);

INSERT INTO metalsPosition(
	metalsHoldingId,
	metalName,
	averageCost,
	initialBuyDate,
	lastUpdateDate,
	totalAmount /* Quantity is measured in oz*/
	) VALUES (
	'0',
	'GOLD',
	1289.4000,
	'2019-01-02 06:45:32',
	'2019-01-02 06:45:32',
	1
);

INSERT INTO metalsChangeRecord(
	metalsHoldingId,
	metalName,
	price,
	changeAmount, /* Quantity is measured in oz */
	changeDate
	) VALUES (
	'0',
	'GOLD',
	1289.4000,
	1,
	'2019-05-10 12:29:00'
);

INSERT INTO currencyPosition(
	currencyHoldingId,
	currencyName,
	averageCost,
	initialBuyDate,
	lastUpdateDate,
	totalAmount
	) VALUES (
	'0',
	'USD',
	NULL,
	'2019-09-07 10:16:48',
	'2019-09-07 10:26:48',
	10000.0000
);

INSERT INTO currencyChangeRecord(
	currencyHoldingId,
	currencyName,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'USD',
	NULL,
	10000.0000,
	'2019-09-07 10:16:48'
);

/*
 * BTC position and record
 */
INSERT INTO currencyPosition(
	currencyHoldingId,
	currencyName,
	averageCost,
	initialBuyDate,
	lastUpdateDate,
	totalAmount
	) VALUES (
	'0',
	'BTC',
	10180.7600,
	'2019-08-22 16:00:48',
	'2019-08-29 15:59:48',
	0.5
);

INSERT INTO currencyChangeRecord(
	currencyHoldingId,
	currencyName,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'BTC',
	10161.1600,
	0.25,
	'2019-08-22 16:00:48'
);

INSERT INTO currencyChangeRecord(
	currencyHoldingId,
	currencyName,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'BTC',
	10068.9200,
	0.25,
	'2019-08-25 15:59:48'
);

INSERT INTO currencyChangeRecord(
	currencyHoldingId,
	currencyName,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'BTC',
	10312.2200,
	0.25,
	'2019-08-26 15:59:48'
);

INSERT INTO currencyChangeRecord(
	currencyHoldingId,
	currencyName,
	price,
	changeAmount,
	changeDate
	) VALUES (
	'0',
	'BTC',
	9487.9800,
	-0.25,
	'2019-08-29 15:59:48'
);

