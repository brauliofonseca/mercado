create table if not exists assets (
	userId integer not null,
	type varchar(20) not null,
	amount numeric(10,2) not null,
	date timestamp without time zone not null,
	name varchar(100) not null
);

