#PostgresDB initialization

Using the following docker commands, you can create an image of a postgres database that will be used by the mercado application. The container will copy in the initialization files, `initTables.sql` and `seed.sql`, which will create the tables and place seed data respectively.

1. Build the image
`docker build -t mercado-db .`
	
2. Get the image ID of the recently built image
`docker image ls`

3. Create a directory to store your data
`mkdir -p $HOME/docker/volumes/postgres/mercado`

4. Execute the run command to create a new container
`docker run --rm --name mercado-db -d -e POSTGRES_USER=mercado-db -e POSTGRES_PASSWORD={yourpassword} -p 5430:5432 -v $HOME/docker/volumes/postgres/mercado:/var/lib/postgresql/data mercado-db` 
5. Can access using this psql command 
`psql -h 0.0.0.0 -U mercado-db --port 5430`
`\c mercado-db`


