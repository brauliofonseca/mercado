/*users*/
create table if not exists users (
	userId varchar(36) not null,
	username varchar(150) not null,
	stockHoldingId varchar(36) not null,
	currencyHoldingId varchar(36) not null,
	metalsHoldingId varchar(36) not null
);


/*stockPosition*/
create table if not exists stockPosition (
	stockHoldingId varchar(36) not null,
	ticker varchar(10) not null,
	averageCost numeric(16,4) not null,
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount integer not null
);

/*stockChangeRecord*/
create table if not exists stockChangeRecord (
	stockHoldingId varchar(36) not null,
	ticker varchar(10) not null,
	price numeric(16,4) not null,
	changeAmount integer not null,
	changeDate timestamptz not null
);

/*metalsPosition*/
create table if not exists metalsPosition (
	metalsHoldingId varchar(36) not null,
	metalName varchar(50) not null,
	averageCost numeric(16,4) not null,
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount numeric(16,2) not null /* Quantity is measured in metric units*/
);

/*metalsChangeRecord*/
create table if not exists metalsChangeRecord (
	metalsHoldingId varchar(36) not null,
	metalName varchar(25) not null,
	price numeric(16,4) not null,
	changeAmount numeric(16,2) not null, /* Quantity is measured in metric units */
	changeDate timestamptz not null
);

/*currencyPosition*/
create table if not exists currencyPosition (
	currencyHoldingId varchar(36) not null,
	currencyName varchar(10) not null,
	averageCost numeric(16,4),
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount numeric(16,4) not null
);

/*currencyChangeRecord*/
create table if not exists currencyChangeRecord (
	currencyHoldingId varchar(36) not null,
	currencyName varchar(10) not null,
	price numeric(16,4),
	changeAmount integer not null,
	changeDate timestamptz not null
);

