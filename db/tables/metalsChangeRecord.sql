/*metalsChangeRecord*/
create table if not exists metalsChangeRecord (
	metalsHoldingId varchar(36) not null,
	metalName varchar(25) not null,
	price numeric(16,4) not null,
	changeAmount numeric(16,2) not null, /* Quantity is measured in metric units */
	changeDate timestamptz not null
);

