create table if not exists currencyPosition (
	currencyHoldingId varchar(36) not null,
	currencyName varchar(10) not null,
	averageCost numeric(16,4),
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount numeric(16,4) not null
);
