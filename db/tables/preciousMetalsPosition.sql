create table if not exists metalsPosition (
	metalsHoldingId varchar(36) not null
	metalName varchar(50) not null
	averageCost money not null
	initialBuyDate timestamp without time zone not null
	lastUpdateDate timestamp without time zone not null
	totalAmount numeric(100, 2) not null /* Quantity is measured in metric units*/
);

