create table if not exists stockChangeRecord (
	stockHoldingId varchar(36) not null,
	ticker varchar(10) not null,
	price numeric(16,4) not null,
	changeAmount integer not null,
	changeDate timestamptz not null
);

