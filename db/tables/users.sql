create table if not exists users (
	userId varchar(36) not null,
	username varchar(150) not null,
	stockHoldingId varchar(36) not null,
	currencyHoldingId varchar(36) not null,
	metalsHoldingId varchar(36) not null
);

