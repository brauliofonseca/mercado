create table if not exists currencyChangeRecord (
	currencyHoldingId varchar(36) not null,
	currencyName varchar(10) not null,
	price numeric(16,4),
	changeAmount integer not null,
	changeDate timestamptz not null
);
