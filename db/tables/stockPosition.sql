create table if not exists stockPosition (
	stockHoldingId varchar(36) not null,
	ticker varchar(10) not null,
	averageCost numeric(16,4) not null,
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount integer not null
);

