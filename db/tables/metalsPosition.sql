create table if not exists metalsPosition (
	metalsHoldingId varchar(36) not null,
	metalName varchar(50) not null,
	averageCost numeric(16,4) not null,
	initialBuyDate timestamptz not null,
	lastUpdateDate timestamptz not null,
	totalAmount numeric(16,2) not null /* Quantity is measured in metric units*/
);
