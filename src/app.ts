import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import asset from "./routes/asset";
import logger from "./util/logger";

// Load environment variables
dotenv.config();

const app = express();
// TODO: Only for development purposes,(access control allow origin) must remove for production env
app.use(cors());

const port = process.env.SERVER_PORT;

app.get("/", (req, res) => {
 res.send("We in business");
});

app.use("/asset", asset);

app.listen(port, () => {
 logger.log("info", "App initialized at http://localhost:%d", port);
});
