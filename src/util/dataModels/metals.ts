import sequelize from "sequelize";
import dbConnection from "../../config/db";

const Model = sequelize.Model;

class Metal extends Model {}
Metal.init({
 averageCost: {
   allowNull: false,
   type: sequelize.DECIMAL
 },
 initialBuyDate: {
   allowNull: false,
   type: sequelize.DATE
 },
 lastUpdateDate: {
   allowNull: false,
   type: sequelize.DATE
 },
 metalName: {
   allowNull: false,
   type: sequelize.STRING
 },
 metalsHoldingId: {
   allowNull: false,
   primaryKey: true,
   type: sequelize.STRING
 },
 totalAmount: {
   allowNull: false,
   type: sequelize.DECIMAL
 },
},
{
 freezeTableName: true,
 modelName: "metalsPosition",
 sequelize: dbConnection,
 tableName: "metalsPosition",
 timestamps: false,
});

export default Metal;
