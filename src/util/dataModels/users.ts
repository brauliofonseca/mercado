import sequelize from "sequelize";
import dbConnection from "../../config/db";
import Currency from "./currencies";
import Metal from "./metals";
import Stock from "./stocks";

const Model = sequelize.Model;
class User extends Model {}

User.init({
currencyHoldingId: {
  allowNull: false,
  field: "currencyHoldingId",
  type: sequelize.UUID,
  get(): string {
      return this.getDataValue("currencyHoldingId");
  }
 },
 metalsHoldingId: {
  allowNull: false,
  field: "metalsHoldingId",
  type: sequelize.UUID,
  get(): string {
      return this.getDataValue("metalsHoldingId");
  }
 },
 stockHoldingId: {
  allowNull: false,
  field: "stockHoldingId",
  type: sequelize.UUID,
  get(): string {
      return this.getDataValue("stockHoldingId");
  }
 },
 userId: {
  allowNull: false,
  primaryKey: true,
  type: sequelize.UUID,
 },
 username: {
  allowNull: false,
  type: sequelize.UUID,
  get(): string {
      return this.getDataValue("username");
  }
 },
},
{
 freezeTableName: true,
 modelName: "users",
 sequelize: dbConnection,
 tableName: "users",
 timestamps: false
});

User.sync();
User.hasMany(Stock, {foreignKey: "stockHoldingId", sourceKey: "stockHoldingId"});
User.hasMany(Currency, {foreignKey: "currencyHoldingId", sourceKey: "currencyHoldingId"});
User.hasMany(Metal, {foreignKey: "metalsHoldingId", sourceKey: "metalsHoldingId"});
export default User;
