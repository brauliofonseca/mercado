import sequelize from "sequelize";
import dbConnection from "../../config/db";

const Model = sequelize.Model;
class Stock extends Model {}
Stock.init({
  averageCost: {
   allowNull: false,
   type: sequelize.DECIMAL,
    get(): number {
        return this.getDataValue("averageCost");
    }
  },
  initialBuyDate: {
   allowNull: false,
   type: sequelize.DATE,
    get(): string {
        return this.getDataValue("initialBuyDate");
    }
  },
  lastUpdateDate: {
   allowNull: false,
   type: sequelize.DATE,
    get(): string {
        return this.getDataValue("lastUpdateDate");
    }
  },
  stockHoldingId: {
   allowNull: false,
   primaryKey: true,
   type: sequelize.UUID,
    get(): string {
        return this.getDataValue("stockHoldingId");
    }
  },
  ticker: {
   allowNull: false,
   type: sequelize.STRING,
    get(): string {
        return this.getDataValue("ticker");
    }
  },
  totalAmount: {
   allowNull: false,
   type: sequelize.NUMBER,
    get(): number {
        return this.getDataValue("totalAmount");
    }
  },
 },
 {
  freezeTableName: true,
  modelName: "stockPosition",
  sequelize: dbConnection,
  tableName: "stockPosition",
  timestamps: false,
 }
);

export default Stock;
