import sequelize from "sequelize";
import dbConnection from "../../config/db";
import User from "./users";

const Model = sequelize.Model;
class Currency extends Model {}
Currency.init(
 {
  averageCost: {
    allowNull: true,
    type: sequelize.DECIMAL,
  },
  currencyHoldingId: {
    allowNull: false,
    primaryKey: true,
    type: sequelize.UUID,
  },
  currencyName: {
    allowNull: false,
    type: sequelize.STRING,
  },
  initialBuyDate: {
    allowNull: false,
    type: sequelize.DATE,
  },
  lastUpdateDate: {
    allowNull: false,
    type: sequelize.DATE,
  },
  totalAmount: {
    allowNull: false,
    type: sequelize.DECIMAL,
  }
 },
 {
  freezeTableName: true,
  modelName: "currencyPosition",
  sequelize: dbConnection,
  tableName: "currencyPosition",
  timestamps: false,
 }
);

Currency.sync();
export default Currency;
