import winston from "winston";

const logger = winston.createLogger({
    format: winston.format.combine(
     winston.format.splat(),
     winston.format.simple(),
    ),
    level: "info",
    transports: [
    new winston.transports.Console()
    ]
});

export default logger;
