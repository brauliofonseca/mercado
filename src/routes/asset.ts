import bodyParser from "body-parser";
import dotenv from "dotenv";
import express from "express";
import * as https from "https";
import * as sprintf from "sprintf-js";
import Currency from "../util/dataModels/currencies";
import Metal from "../util/dataModels/metals";
import Stock from "../util/dataModels/stocks";
import User from "../util/dataModels/users";
import logger from "../util/logger";

dotenv.config();

const router = express.Router();
router.use(express.json());

const dbPort = parseInt(process.env.POSTGRES_DB_PORT, 10);

/* Middleware */
router.use(bodyParser.json());

enum AssetType {
    COMMODITY = "",
    CRYPTO = "CURRENCY_EXCHANGE_RATE",
    STOCK = "GLOBAL_QUOTE"
}
enum TimeFrame {
    current = "",
    day = "TIME_SERIES_DAILY",
    month = "TIME_SERIES_MONTHLY",
    week = "TIME_SERIES_WEEKLY"
}

const STOCK_API_ENDPOINT = "https://www.alphavantage.co/query?function=%s&symbol=%s&interval=%s\
&outputsize=full&apikey=%s";

const CRYPTO_API_ENDPOINT = "https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=%s\
&to_currency=USD&apikey=%s";

const COMMODITY_API_ENDPOINT = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT\
&interval=5min&outputsize=full&apikey=%s";

// TODO: Must create a middleware function to validate input data

/* Query parameters
 	type = string value of asset (ie. gold, bitcoin, spy)
  timeframe = integer value to show price through a specified time frame
		(constant values ranging from current to 5 months)
 */
router.get("/price", (req: any, res: any) => {

 // TODO: The code here is very convoluted, needs refactoring
 type assetType = "COMMODITY" | "CRYPTO" | "STOCK";
 type timeFrame = "current" | "day" | "month" | "week";
 const assetName: string = req.query.name;
 const assetTypeName: assetType = req.query.type;
 const timeFrameName: timeFrame = req.query.timeFrame;
 const assetType: string =  AssetType[assetTypeName];
 const timeFrameParam: string = TimeFrame[timeFrameName];
 const requestUrl = sprintf.vsprintf(STOCK_API_ENDPOINT, [assetType, assetName,
                                     timeFrameParam, process.env.ALPHA_ADVANTAGE]);

 // let assetApiResponse: string;
 // Use HTTPS due to the API we are using
 https.get(requestUrl, (response) => {
  logger.info("URL: %s", requestUrl);
  logger.info("Status code: %d", response.statusCode);
  logger.info("Header: %s", response.headers);
  response.on("data", (data) => {
      logger.info("Data: %s", JSON.stringify(data));
      res.status(200).json(data);
  });
 }).on("error", (error) => {
      logger.error("Incorrect request sent: %s", error);
 });
});

/*
 Get all user assets
  Query: /assets?id={userId}
  Response: {
   stocks: [
    {Stock object}
   ],
   currency: [
    {Currency object}
   ],
   metals: [
    {Metal object}
   ],
  }
*/
interface IAssets {
    Currency: Currency[];
    Metals: Metal[];
    Stocks: Stock[];
}

router.get("/", (req, res) => {
    const currentUserId = req.query.id;
    logger.info("User ID: %s", currentUserId);
    const allAssets = {} as IAssets;

    User
        .findByPk(req.query.id)
        .then((user: User) => {
            logger.info("StockHoldingId value: " + user.get("stockHoldingId"));
            return Stock.findAll({
                where: {
                    stockHoldingId: user.get("stockHoldingId")
                }
            });
        })
        .then((stocks: Stock[]) => {
            allAssets.Stocks = stocks;
            return allAssets;
        })
        .then((assets: IAssets) => {
            logger.info("Done with promises!");
            logger.info("All assets: " + JSON.stringify(allAssets));
            return res.status(200).json(assets);
        });

});

 /* Update user's assets
 		Endpoint: /asset
	   Req body: { name: "", type: "", amount: ""}
 */
router.post("/", (req, res) => {

 const assetName = req.body.name;
 const type = req.body.type;
 const quantity = req.body.quantity;
 res.status(200);
});

export default router;
