import dotenv from "dotenv";
import sequelize from "sequelize";
import logger from "../util/logger";

dotenv.config();

const dbConnection = new sequelize.Sequelize(
 process.env.POSTGRES_DB_NAME,
 process.env.POSTGRES_USERNAME,
 process.env.POSTGRES_PASSWORD,
 {
  dialect: "postgres",
  host: process.env.POSTGRES_DB_HOST,
  pool: {
   acquire: Number(process.env.MAX_ACQUIRE_DB_CONNECTION_TIME),
   idle: Number(process.env.MAX_IDLE_DB_CONNECTION_TIME),
   max: Number(process.env.MAX_DB_CONNECTION),
   min: Number(process.env.MIN_DB_CONNECTION),
  },
  port: Number(process.env.POSTGRES_DB_PORT),
  quoteIdentifiers: false,
 }
);

// Test DB connection
dbConnection.authenticate()
  .then(() => logger.info("Connection is established with database..."))
  .catch((err) => {
   logger.error("Error when connecting to database...");
   throw err;
  });

// registry plugin will remove circular dependencies between models
// bookshelf.plugin("registry");
export default dbConnection;
